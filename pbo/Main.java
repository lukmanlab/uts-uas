import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
  public static void main(String[] args) {

    HashMap<String, Pelanggan> mapPlg = new HashMap<String, Pelanggan>();

    Scanner sc = new Scanner(System.in);
    int input = 0;

    do {
      System.out.println();
      System.out.println("1. Login Pelanggan");
      System.out.println("2. Login Admin / Engineer");
      System.out.println("0. Exit");
      System.out.print("Pilih: ");
      input = sc.nextInt();
      System.out.print("\033[H\033[2J");
      switch (input) {
        case 1:
          String idPelanggan;
          System.out.printf("Login by ID: ");
          idPelanggan = sc.next();

          if (mapPlg.get(idPelanggan) == null){
            System.out.println("\n ==> DATA KOSONG !!!\n");
            break;
          }else if (idPelanggan.equals(mapPlg.get(idPelanggan).getId())){
            System.out.printf("x===============================x\n");
            System.out.printf("|     ID     |     Username     |\n");
            System.out.printf("|===============================|\n");
            System.out.printf("| %-10s | %-16s |\n", mapPlg.get(idPelanggan).getId(), mapPlg.get(idPelanggan).getUsername());
            System.out.printf("x===============================x\n\n");
            System.out.printf("x================================================================x\n");
            System.out.printf("|   Tagihan   |           Tanggal           |        Jenis       |\n");
            System.out.printf("|================================================================|\n");
            for (Tagihan plg : mapPlg.get(idPelanggan).getTagihan()) {
              System.out.printf("| %-11.2f | %-27s | %-18s |\n", plg.getTotalTagihan(), plg.getTimestamp(), plg.getTipe());
            }
            System.out.printf("x================================================================x\n");
            break;
          }else{
            System.out.println("\n ==> LOGIN GAGAL !!!\n");
            break;
          }
          
        case 2:
          String password;
          System.out.print("Masukkan PASSWORD: ");
          password = sc.next();
          if (password.equals("PBO")){
            do {
              System.out.println("\nHalo Admin / Engineer");
              System.out.println("=====================");
              System.out.println("1. Add Pelanggan");
              System.out.println("2. Lihat Pelanggan");
              System.out.println("3. Tambahkan Tagihan ");
              System.out.println("9. Kembali");
              System.out.print("Pilih: "); 
              input = sc.nextInt();
              System.out.print("\033[H\033[2J");
              System.out.println();
              switch (input) {
                case 1:
                  String id; String username;
                  System.out.print("Daftarkan ID: ");
                  id = sc.next();
                  System.out.print("Masukkan Username: ");
                  username = sc.next();
                  mapPlg.put(id, new Pelanggan(id,username));
                  break;
  
                case 2:
                  System.out.printf("x===============================x\n");
                  System.out.printf("|     ID     |     Username     |\n");
                  System.out.printf("x===============================x\n");
                  for(Map.Entry b: mapPlg.entrySet()){
                    Pelanggan plg = (Pelanggan) b.getValue();
                    System.out.printf("| %-10s | %-16s |\n", plg.getId(), plg.getUsername());
                  }
                  System.out.printf("x===============================x\n");
                  break;
  
                case 3:
                  String idPlg;
                  System.out.printf("Masukkan ID Pelanggan: ");
                  idPlg = sc.next();
  
                  if (mapPlg.get(idPlg) == null){
                    System.out.println("\n ==> DATA KOSONG !!!\n");
                    break;
                  }else if(idPlg.equals(mapPlg.get(idPlg).getId())){
                    int pil;
                    System.out.println("1. Tagihan Listrik");
                    System.out.println("2. Tagihan Air");
                    System.out.printf("Pilih: "); 
                    pil = sc.nextInt();
                    switch (pil) {
                      case 1:
                        double kWh;
                        System.out.printf("Masukkan kWh: ");
                        kWh = sc.nextDouble();
                        mapPlg.get(idPlg).addTagihan(new Listrik(kWh, 1400));
                        break;
  
                      case 2:
                        double m;
                        System.out.printf("Masukkan Meter: ");
                        m = sc.nextDouble();
                        mapPlg.get(idPlg).addTagihan(new Air(m, 1000));
                        break;
  
                      default:
                        break;
                    }
                  }
  
                  break;
  
                default:
                  break;
              }
            } while (input != 9);
          }else{
            System.out.println("\n ==> LOGIN GAGAL!!!\n");
            break;
          }
        
        default:
          break;
        }

    } while (input != 0);

    sc.close();

  }
}
