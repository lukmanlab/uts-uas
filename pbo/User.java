interface User {

  public String getId();

  public String getUsername();

  public String getData();

  public String getType();
}