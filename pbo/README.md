# UTS Pemograman Berbasis Objek
Program Billing Listrik dan Air menggunakan bahasa Java (OOP)

## Persyaratan
- Java JDK 15

```
 $ java -version
java version "15.0.1" 2020-10-20
Java(TM) SE Runtime Environment (build 15.0.1+9-18)
Java HotSpot(TM) 64-Bit Server VM (build 15.0.1+9-18, mixed mode, sharing)
```

## Penggunaan

- Compile
```
javac *
```

- Jalankan
```
java Main
```

Output:

![result](src/output1.png)