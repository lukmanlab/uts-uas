import java.util.ArrayList;

public class Pelanggan implements User{
  private String idUser;
  private String userName;
  private String dataUser;
  private ArrayList<Tagihan> listTagihan = new ArrayList<Tagihan>();

  public Pelanggan(String idUser, String userName){
    this.idUser = idUser;
    this.userName = userName;
  }

  public String getId(){
    return idUser;
  }

  public String getUsername() {
    return userName;
  }

  public String getData() {
    return dataUser;
  }

  public String getType() {
    return "pelanggan";
  }

  public void addTagihan(Tagihan tagihan) {
    listTagihan.add(tagihan);
  }

  public ArrayList<Tagihan> getTagihan() {
    return listTagihan;
  }
}
