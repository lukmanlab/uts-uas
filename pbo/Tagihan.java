import java.sql.Timestamp;

class Tagihan {
  private int tarif;
  private double totalBiaya;
  private String tipe;
  private Timestamp timestamp;

  public Tagihan(double meteran, int tarif){
    this.tarif = tarif;
    this.timestamp = new Timestamp(System.currentTimeMillis());
    this.totalBiaya = this.tarif * meteran;
  }

  public int getTarif(){
    return this.tarif;
  }

  public double getTotalTagihan() {
    return this.totalBiaya;
  }

  public String getTipe() {
    return this.tipe;
  }

  public Timestamp getTimestamp() {
    return this.timestamp;
  }

  public void setTipe(String tipe){
    this.tipe = tipe;
  }

  public void setTarif(int tarif) {
    this.tarif = tarif;
  }
  
}

class Listrik extends Tagihan{

  public Listrik(double meteran, int tarif){
    super(meteran,tarif);
    this.setTipe("listrik");
    this.setTarif(tarif);
  }

}

class Air extends Tagihan{

  public Air(double meteran, int tarif){
    super(meteran,tarif);
    this.setTipe("air");
    this.setTarif(tarif);
  }

}